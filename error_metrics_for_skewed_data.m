function [precision recall F1_score] = error_metrics_for_skewed_data(theta, X, y, ...
    model, ...
    tail_label)

% Func: error_metrics_for_skewed_data()
%
% Parameter:
%   theta -- the parameters of model, for logistic regression, n+1 × 1
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the labels
%       m × num_labels should be binary value (0/1)
%   model -- an anonymous functions of model whose paramters are theta and X, return predictive value
%   tail_label -- a vector, specify the label with the fewest number
%
% Do:
%   precision = [true positive] / [no. of predicted positive]
%   recall = [true positive] / [no. of actual positive],
%       while true positive = predicted label == actual label,
%   F1_score = 2 * (P * R) / (P + R)
%
% Return: precision recall
%   F1_score -- to trade off between precision and recall

    precision = 0;
    recall = 0;
    F1_score = 1;

    threshold = 0.5;

    if size(y)(2) == 1
        actual_label = y;
        pred_label = model(theta, X) >= threshold;
    else
        [val actual_label] = max(y');
        [val pred_label] = max(model(theta, X)');
        [val tail_label] = max(tail_label);
    end

    predicted_positve = sum(pred_label == tail_label);
    actual_positive = sum(actual_label == tail_label);
    true_positive = sum((actual_label == tail_label) & (tail_label == pred_label));

    precision = true_positive / predicted_positve;
    recall = true_positive / actual_positive;

    F1_score = 2 * (precision * recall) / (precision + recall);

end
