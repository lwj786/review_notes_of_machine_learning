function [X_reduce variance_retained U S] = principal_component_analysis(X, K)

% Func: principal_component_analysis()
%
% Parameter:
%   X -- m × n matrix, training data set
%       data should be performed mean normalization, and optionally feature scaling as well
%   K -- the dimensionality after reduction
%
% Do:
%   use PCA to reduce dimensionality and compute how much of variance is retained
%
% Reuturn: X_reduce variance_retained U S
%   U, S -- the first two return value of svd()

    [m n] = size(X);

    X_reduce = X;
    variance_retained = 1;
    U = zeros(n);
    S = zeros(n);

    Sigma = 1/m * X' * X;
    [U S V] = svd(Sigma);
    X_reduce = X * U(:, 1:K);

    variance_retained = sum(sum(S(1:K, 1:K))) / sum(sum(S));

end
