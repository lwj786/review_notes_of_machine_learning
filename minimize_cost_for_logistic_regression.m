function [theta cost exit_flag] = minimize_cost_for_logistic_regression(initial_theta, X, y, iteration_num)

% Func: minimize_cost_for_logistic_regression(initial_theta, X, y)
%
% Parameter:
%   initial_theta -- the initial parameters of hypothesis function
%       n+1 × 1 matrix
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value
%       m × 1 matrix
%   iteration_num -- number of Max iteration
%
% Do: use optimization algorithm to minimize cost for logistic regression:
%   get cost and gradient with cost_and_gradient_for_logistic_regression()
%   and use fminunc to minimize cost
%
% Return: theta cost exit_flag

    theta = zeros(size(initial_theta));
    cost = 0;
    exit_flag = 0;

    options = optimset('GradObj', 'on', 'MaxIter', iteration_num);

    [theta cost exit_flag] = ...
        fminunc(@(t)(cost_and_gradient_for_logistic_regression(t, X, y)), ...
            initial_theta, ...
            options);

end
