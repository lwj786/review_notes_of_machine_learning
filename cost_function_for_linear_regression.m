function J = cost_function_for_linear_regression(theta, X, y, ...
    lambda = 0)

% Func: cost_function_for_linear_regression(theta, X, y, lambda)
%
% Parameter:
%   theta -- the parameters of linear function: hypothesis
%       n+1 × 1 matrix
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value
%       m × 1 matrix
%   lambda -- the regularization parameter (optional, default 0)
%
% Do:
%   J = 1 / (2 * m) * Σ[(h(x_i) - y_i) ^ 2] + lambda / (2 * m) * Σ[theta_j^2],
%       i = 1 -> m, j = 1 -> n
%   where h(x) = X * theta
%
% Return: J

    m = length(y);    % the number of examples
    J = 0;

    J = 1 / (2 * m) * sum((X * theta - y) .^ 2) ...
        + lambda / (2 * m) * sum(theta(2:end, :) .^ 2);   % regular term, not regularize the theta_0

end
