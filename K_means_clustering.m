function [clusters centroids] = K_means_clustering(K, X, iteration, ...
    centroids = [])

% Func: K_means_clustering()
%
% Parameter:
%   K -- number of clusters, < m
%   X -- m × n matrix, training set
%   iteration -- number of iteration
%   centroids -- initialize centroids manually, optional
%
% Do: choose K cluster centroid (randomly), assign all points to each closest centroid
%   then iteration: compute the new centroid and reassign
%
% Return:
%   clusters -- K × 1, cell
%   centroids -- K × n, cluster centroids

    [m n] = size(X);

    if K <= 0 || K > m
        K = m;
    end

    clusters = {[]};
    for iter = 2:K
        clusters = [clusters; []];
    end

    % choose K cluster centroid randomly
    if ! length(centroids)
        m_divide = floor(m / K);
        k = zeros(K, 1);
        for iter = 1:K
            k(iter) = (iter - 1) * m_divide;

            if iter == K && mod(m, K) ~= 0
                m_divide = mod(m, K);
            end

            k(iter) = k(iter) + ceil(rand(1) * m_divide);
        end
        centroids = X(k, :);
    end

    % K-means clustering
    index_record = zeros(m, 1);
    for iter = 1:iteration
        for i = 1:m
            [val index_record(i)] = min(sum((X(i, :) - centroids)' .^ 2));
        end

        for j = 1:K
           centroids(j, :) = sum((index_record == j) .* X) / sum(index_record == j);
        end
    end

    for i = 1:m
        clusters{index_record(i)} = [clusters{index_record(i)}; X(i, :)];
    end

end
