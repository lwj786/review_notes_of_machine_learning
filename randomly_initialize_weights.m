function nn_params = randomly_initialize_weights(input_layer_size, hidden_layer_size, num_labels, epsilon)

% Func: randomly_initialize_weights()
%
% Parameter:
%   input_layer_size -- number of iuput units, should be a number
%   hidden_layer_size -- could be a number or vector
%       the length is number of hidden layers, and each value is number of units
%   num_labels -- or output layer size, should be a number
%   epsilon -- decide the range of weights
%
% Do:
%   initialize each weight(or theta, neural networks parameter) to a random value in [-epsilon, epsilon]
%
% Return: nn_params

    nn_params = 0;

    layer_size = [input_layer_size(:); hidden_layer_size(:); num_labels(:)];
    num_layers = length(layer_size);
    num_units = 0;

    for l = 1:(num_layers - 1)
        num_units = num_units + (layer_size(l) + 1) * layer_size(l + 1);    % +1 for theta_0 or bias
    end

    nn_params = rand(1, num_units)(:) * 2 * epsilon - epsilon;

end
