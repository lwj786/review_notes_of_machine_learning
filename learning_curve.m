function [error_train error_cv] = learning_curve(X, y, ...
    ind, rule, ...
    train_model, ...
    model_evaluation)

% Func: learning_curve()
%
% Parameter:
%   X -- a cell, {X_train; X_cv}, the feature of data set
%       for each X, m × n+1 matrix, the first column x_0 should be 1
%   y -- a cell, {y_train; y_cv}, the target value or labels
%       for each y, m × 1 matrix, for classification, m × num_labels, should be binary value (0/1)
%
%   ind -- independent variable, set variable you want to observe it's impact on model error
%   rule -- the rule to change ind
%       ind <-> rule
%       'm' -- the amount of data set
%       <-> [number] -- the space, default 1 (means if it <= 0 or >= m, it will be 1)
%       e.g. the amout is 1000, and space is 50, it will train 20 models
%
%       'f' -- the feature X
%       <-> [cell] -- {map_feature; num}, anonymous functions whose parameter are feature X, the order of model and extra parameter opt,
%           return feature X and
%           *** also return opt, you could use it to transfer message from X_train to X_cv, such as normalization paramter ***
%       e.g. to map feature to polynomial feature from 1 to 10, set {poly_feature; 10}
%
%       'p' -- the extra parameter of train_model(), *** if it setted, the train_model should have three parameter ***
%       <-> [vector] -- a list of number
%       e.g. could be alpha, the step size of gradient descent OR lambda, the regularization parameter
%
%   train_model -- an anonymous function to train model whose paramters are X and y, return theta
%   model_evaluation -- an anonymous functions to evaluate model whose paramters are theta, X and y, return error
%
% Do: train a series of models according to the parameter var and it's rule,
%   then compute their error using evaluating_model()
%
% Return:
%   error_train
%   error_cv -- cv means cross validation

    error_train = 1;
    error_cv = 1;

    theta = [];
    X_train = X{1};
    X_cv = X{2};
    y_train = y{1};
    y_cv = y{2};
    m = size(X_train, 1);

    m_list = [];
    map_feature = @(p, n, opt) p;
        opt = [];
    param = [];
    n = 0;

    % ind ~~ rule
    if strcmp(ind, 'm')
        if rule <= 0 || rule >= m
            rule = 1
        end
        m_list = rule:rule:m;
        if m_list(length(m_list)) ~= m
            m_list = [m_list m]
        end

        n = length(m_list);
    elseif strcmp(ind, 'f')
        map_feature = rule{1};
        n = rule{2};
    elseif strcmp(ind, 'p')
        param = rule(:);
        n = length(param);
    else
        printf("ERROR: there is NO such parameter: %s\n", ind);
        return
    end

    error_train = ones(1, n);
    error_cv = ones(1, n);

    %
    if strcmp(ind, 'm')
        for k = 1:n
            m = m_list(k);

            theta = train_model(X_train(1:m, :), y_train(1:m));

            error_train(k) = ...
                model_evaluation(theta, X_train(1:m, :), y_train(1:m));
            error_cv(k) = ...
                model_evaluation(theta, X_cv, y_cv);
        end
    elseif strcmp(ind, 'f')
        for k = 1:n
            [X_train_mapped opt] = map_feature(X_train, k, opt = []);
            theta = train_model(X_train_mapped, y_train);

            error_train(k) = ...
                model_evaluation(theta, X_train_mapped, y_train);
            error_cv(k) = ...
                model_evaluation(theta, map_feature(X_cv, k, opt), y_cv);
        end
    else
        for k = 1:n
            theta = train_model(X_train, y_train, param(k));

            error_train(k) = ...
                model_evaluation(theta, X_train, y_train);
            error_cv(k) = ...
                model_evaluation(theta, X_cv, y_cv);
        end
    end

end
