function X_recover = recover_data(X_reduce, U)

% Func: recover_data()
%
% Parameter:
%   X -- m × K matrix, the data set after dimensionality reduction
%   U -- the first return value of svd()
%
% Do:
%   X_recover = X_reduce * U(:, 1:K)'
%
% Return: X_recover

    K = size(X_reduce, 2);

    X_recover = X_reduce * U(:, 1:K)';

end
