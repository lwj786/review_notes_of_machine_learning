function g = sigmoid(z)

% Func: sigmoid(z)
%
% Parameter: z
%   z could also be vectors and matrices
%
% Do: g = 1 / (1 + exp(-z))
%
% Return: g

    g = zeros(size(z));

    g = 1 ./ (1 + exp(-z));

end
