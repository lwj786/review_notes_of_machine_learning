function  [mu sigma2] = gaussian_distribution_for_density_estimation(X)

% Func: gaussian_distribution_for_density_estimation()
%
% Parameter:
%   X -- m × n matrix, training data set
%
% Do:
%   use gaussian distribution to estimate the distribution of training data set,
%   may better to map X first.
%   use method of moments to estimate the parameter
%   * method of moments:
%       mu_j = 1/m * Σx_j(i), i = 1 -> m
%       sigma2_j = 1/(m-1) * Σ(x_j(i) - mu_j)^2, i = 1 -> m 
%
% return:
%   mu -- mean, sigma2 -- variance

    [m n] = size(X);

    mu = zeros(1, n);
    sigma2 = zeros(1, n);

    mu = mean(X);
    sigma2 = 1/(m - 1) * sum((X - mu) .^ 2);

end
