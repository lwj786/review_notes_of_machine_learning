function [J grad] = neural_networks_cost_and_gradient_function(nn_params, ...
    input_layer_size, hidden_layer_size, num_labels, ...
    X, y, lambda = 0)

% Func: neural_networks_cost_and_gradient_function()
%
% Parameter:
%   nn_params -- or weights, a column vector
%   input_layer_size -- number of iuput units, should be a number
%   hidden_layer_size -- could be a number or vector
%       the length is number of hidden layers, and each value is number of units
%   num_labels -- or output layer size, should be a number
%   X -- the feature of data set, m × n matrix
%   y -- the labels, m × num_labels matrix,
%       should be mapped to binary vector of 1's and 0's
%   lambda -- the regularization parameter (optional, defaut 0)
%
% Do: compute cost and use Backpropagation algorithm to compute gradient
%
% Return: J grad

    J = 0;
    grad = zeros(size(nn_params));

    m = size(X, 1);
    layer_size = [input_layer_size(:); hidden_layer_size(:); num_labels(:)];
    num_layers = length(layer_size);

    % Forward propagation
    a = {[ones(m, 1) X]};
    Theta = { ...
        reshape(nn_params(1:(layer_size(1)  + 1) * layer_size(2)), ...
        layer_size(1) + 1, layer_size(2)) ...
    };
    Theta_num = (layer_size(1) + 1) * layer_size(2);

    for l = 2:num_layers
        a_l = sigmoid(a{l - 1, 1} * Theta{l - 1, 1});

        if l ~= num_layers
            a = [a; [ones(m, 1) a_l]];    % the extra ones vector is bias
        else
            a = [a; a_l];
        end

        if l ~= num_layers
            Theta_l = ...
                reshape(nn_params(Theta_num + 1:Theta_num + (layer_size(l) + 1) * layer_size(l + 1)), ...
                    layer_size(l) + 1, layer_size(l + 1));
            Theta = [Theta; Theta_l];
            Theta_num = Theta_num + (layer_size(l) + 1) * layer_size(l + 1);
        end
    end

    % the cost J (without regularization) is the sum
    % of logistic regression costs for each cell in the output layer
    J = (1/m) * sum(sum(-y .* log(a{num_layers, 1}) - (1 - y) .* log(1 - a{num_layers, 1})));

        % regular term, the sum of each thetai's(weight) squares expect weight's for bias
    regular_term = 0;
    for l = 1:(num_layers - 1)
        regular_term = regular_term + sum(sum(Theta{l, 1}(2:end, :) .^ 2));
    end

    J = J + lambda / (2 * m) * regular_term;

    % Backpropagation
    delta_l = a{num_layers, 1} - y;
    for l = (num_layers - 1):-1:1
        % the delta_l in computation actually is delta in (l + 1) layer
        Delta_l = delta_l' * a{l, 1};

        grad(Theta_num - (layer_size(l) + 1) * layer_size(l + 1) + 1:Theta_num) = ...
            (1/m) * (Delta_l' + lambda * [zeros(1, layer_size(l + 1)); Theta{l, 1}(2:end, :)])(:);
        Theta_num = Theta_num - (layer_size(l) + 1) * layer_size(l + 1);

        % the delta_l in computation actually is delta in (l + 1) layer
        % a{l, 1} * (1 - a{l, 1}) is the g-prime,
        % which is the derivative of the activation function g
        % evaluated with the input values given by a{l - 1, 1} * Theta{l - 1, 1}
        delta_l = delta_l * Theta{l, 1}' .* a{l, 1} .* (1 - a{l, 1});
        delta_l = delta_l(:, 2:end);   % drop the delta for bias
    end

end
