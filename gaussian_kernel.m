function similarity = gaussian_kernel(X, L, sigma)

% Func: gaussian_kernel()
%
% Parameter:
%   X -- the set of points in hyperplane
%   L -- landmark points, usually just use X
%   sigma -- the parameter of gaussian function
%
% Do:
%   similarity_i,j = exp(- ||x_i - l_j||^2 / (2 * sigma^2))
%
% Return: similarity

    m = size(X, 1);
    m_l = size(L, 1);

    similarity = zeros(m, m_l);

    for index = 1:m
        similarity(index, :) = exp(- sum(((X(index, :) - L)') .^ 2) / (2 * sigma^2));
    end

end
