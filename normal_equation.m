function theta = normal_equation(X, y, ...
    lambda = 0)

% Func: normal_equation(X, y, lambda)
%
% Parameter:
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value
%       m × 1 matrix
%   lambda -- the regularization parameter (optional, default 0)
%
% Do:
%   theta = pinv(X' * X + lambda * L) * X' * y
%       where L is a unit matrix but the first element is 0
%   pinv(), a function in Octave calculate inverse and
%   can give a value even if the matrix is not invertible
%
% Return: theta

    theta = zeros(1, size(X, 2));
    L = eye(size(X, 2));
        L(1, 1) = 0;    % not regularize theta_0

    theta = pinv(X' * X + lambda * L) * X' * y;

end
