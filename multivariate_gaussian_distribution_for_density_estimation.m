function [mu C] = multivariate_gaussian_distribution_for_density_estimation(X)

% Func: multivariate_gaussian_distribution_for_density_estimation()
%
% Parameter:
%   X -- m × n matrix, training data set
%
% Do:
%   estimate the parameter of multivariate gaussian distribution for data
%   may better to map X first.
%   use sample covariance matrix to estimate covariance matrix
%
% Return:
%   mu -- mean, C -- covariance matrix

    [m n] = size(X);

    mu = zeros(1, n);
    C = zeros(n);

    mu = mean(X);

    for i = 1:n
        for j = i:n
            C(i, j) = 1/(m - 1) * (X(:, i) - mean(X(:, i)))' * (X(:, j) - mean(X(:, j)));
        end
    end

    for i = 1:n
        for j = 1:(i - 1)
            C(i, j) = C(j, i);
        end
    end

end
