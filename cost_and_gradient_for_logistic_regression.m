function [J grad] = cost_and_gradient_for_logistic_regression(theta, X, y, ...
    lambda = 0)

% Func: cost_and_gradient_for_logistic_regression(theta, X, y, lambda)
%
% Parameter:
%   theta -- the parameters of hypothesis function
%       n+1 × 1 matrix
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value
%       m × 1 matrix
%   lambda -- the regularization parameter (optional, default 0)
%
% Do: compute the cost of logistic regression and the gradient of cost
%   J = 1/m * Σ[-y_i * log(h(x_i)) - (1 - y_i) * log(1 - h(x_i))]
%       + lambda / (2 * m) * Σ[theta_j^2],
%           i = 1 -> m, j = 1 -> n
%
%   grad = 1/m * Σ[(h(x_i) - y_i) * x_i,j] + lambda / m * theta_j,
%       i = 1 -> m, j = 0 -> n (in regular term j = 1 -> n, not regularize the theta_0)
%   where, h(x) = sigmoid(X * theta)
%
% Return: J grad

    m = length(y);    % the number of exsamples
    J = 0;
    grad = zeros(size(theta));

    J = (1/m) * sum(-y .* log(sigmoid(X * theta)) - (1 - y) .* log(1 - sigmoid(X * theta))) ...
        + lambda / (2 * m) * sum(theta(2:end, :) .^ 2);   % regular term, not regularize the theta_0

    grad = (1/m) * X' * (sigmoid(X * theta) - y) ...
        + [0; lambda / m * theta(2:end, :)];

end
