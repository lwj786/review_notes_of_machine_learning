function model_error = evaluating_model(theta, X, y, ...
    model_type, ...
    model = @(p, x) x * p)

% Func: evaluating_model()
%
% Parameter:
%   theta -- the parameters of model, for 'lr' and 'c', n+1 × 1
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value or labels
%       m × 1 matrix, for classification, m × num_labels should be binary value (0/1)
%   model_type -- set the type of model:
%       'lr' -- linear regression,
%       'c' -- classification (refer in particular to logistic regression),
%       'ann' -- artificial neural network
%   *** for simplicity, only consider 0/1 classification for logistic regression ***
%   model -- an anonymous functions of model whose paramters are theta and X
%       if model_type is 'lr' or 'c', this parameter is optional.
%
% Do: compute error according to the model type
%
% return: model_error

    model_error = 1;

    m = size(X, 1);
    threshold = 0.5;
    pred = [];

    if strcmp(model_type, 'lr') && strcmp(model_type, 'c') && strcmp(model_type, 'ann')
        printf("ERROR: NO such type: %s", model_type);
        return
    end

    if nargin == 4
        if strcmp(model_type, 'c')
            model = @(p, x) sigmoid(x * p);
        elseif strcmp(model_type, 'ann')
            printf("ERROR: Didn't set parameter: model\n");
            return
        end
    end

    %
    if strcmp(model_type, 'lr')
        model_error = 1 / (2 * m) * sum((model(theta, X) - y) .^ 2);
    elseif strcmp(model_type, 'c')
        pred = model(theta, X) >= threshold;
        model_error = sum(pred ~= y) / m;
    elseif strcmp(model_type, 'ann')
        [val label]= max(y');
        [val pred_label] = max(model(theta, X)');

        model_error = sum(pred_label' ~= label') / m;
    end

end
