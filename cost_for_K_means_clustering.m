function J = cost_for_K_means_clustering(clusters, centroids)

% Func: cost_for_K_means_clustering()
%
% Parameter:
%   clusters -- K × 1, cell
%   centroids -- K × n, cluster centroids
%
% Do:
%   cost = 1/m * sum of the square of the distance of each point to its centroid
%
% Return: J

    J = 0;

    K = size(clusters, 1);
    m = 0;

    for i = 1:K
        m = m + size(clusters{i}, 1);
        J = J + sum(sum((clusters{i} - centroids(i)) .^ 2));
    end

    J = J / m;

end
