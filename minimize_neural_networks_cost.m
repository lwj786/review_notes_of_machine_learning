function [nn_params cost exut_flag] = minimize_neural_networks_cost(initial_nn_params, ...
    input_layer_size, hidden_layer_size, num_labels, ...
    X, y, ...
    iteration_num, ...
    lambda = 0)

% Func: minimize_neural_networks_cost()
%
% Parameter:
%   initial_nn_params -- or weights, a column vector
%   input_layer_size -- number of iuput units, should be a number
%   hidden_layer_size -- could be a number or vector
%       the length is number of hidden layers, and each value is number of units
%   num_labels -- or output layer size, should be a number
%   X -- the feature of data set, m × n matrix
%   y -- the labels, m × num_labels matrix,
%       should be mapped to binary vector of 1's and 0's
%   iteration_num -- number of Max iteration
%   lambda -- the regularization parameter (optional, default 0)
%
% Do: use optimization algorithm(fminunc() here) to minimize cost of neural networks
%
% Return: nn_params cost exit_flag

    nn_params = zeros(size(initial_nn_params));
    cost = 0;
    exit_flag = 0;

    options = optimset('GradObj', 'on', 'MaxIter', iteration_num);

    cost_function = @(p) neural_networks_cost_function(p, ...
        input_layer_size, hidden_layer_size, num_labels, ...
        X, y, ...
        lambda);

    [nn_params cost exit_flag] = ...
        fminunc(cost_function, initial_nn_params, options);

end
