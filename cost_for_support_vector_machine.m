function J = cost_for_support_vector_machine(theta, X, y, ...
    C = 1, ...
    kernel = @(X) X)

% Func: cost_for_support_vector_machine()
%
% Parameter:
%   theta -- the parameters of hypothesis function
%       n+1 × 1 matrix
%   X -- the feature of data set
%       m × n matrix, the first column x_0 should be 1
%   y -- the target value, binary value 0/1
%       m × 1 matrix
%   C -- regularization parameter, 1/lambda
%       optional, default 1 (if you want set kernel, C should be setted)
%   kernel -- anonymous function or function handle, which paramter is X, and return new feature
%       optional, default as the linear kernel
%
% Do: compute the cost of svm
%
% Return: J

    J = 0;

    m = size(X, 1);
    X = [ones(m, 1) kernel(X)];
    y = 2 * y - 1;    % map 0/1 to -1/1

    J = C * sum(max([zeros(m, 1) 1 - y .* (X * theta)]')) + 1/2 * sum(theta .^ 2);

end
