function  [diff analytic_grad numerical_grad] = check_gradients(cost_function, Theta, ...
    epsilon = 1e-4)

% Func: check_gradients()
%
% Parameter:
%   cost_function -- an anonymous function with single parameter
%       (it means the parameter of Octave functions here), Theta,
%       return cost and gradient
%   Theta -- the parameters of hypothesis function, a column vector
%   epsilon -- the parameter to compute numerical gradient(optional, default 1e-4)
%
% Do: compute the analytic gradient using neural_networks_cost_and_gradient_function() and
%   numerical gradient, which should be [f(t + h) - f(t - h)] / (2*h), h -> 0
%   then compute their difference
%
% Return: 
%   diff -- norm(analytic_grad - numerical_grad) / norm(analytic_grad + numerical_grad)
%   analytic_grad numerical_grad

    diff = 1;

    [cost analytic_grad] = cost_function(Theta);

    n = length(Theta);
    for k = 1:n
        h = zeros(n, 1);
        h(k, 1) = epsilon;

        cost_l = cost_function(Theta + h);
        cost_r = cost_function(Theta - h);

        numerical_grad(k, 1) = (cost_l - cost_r) / (2 * epsilon);
    end

    diff = norm(analytic_grad - numerical_grad) / norm(analytic_grad + numerical_grad);

end
