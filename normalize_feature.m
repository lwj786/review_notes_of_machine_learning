function [normal_X avg sigma]= normalize_feature(X)

% Func: normalize_feature(X)
%
% Parameter:
%   X -- the feature of data set
%       m × n matrix, the first column x_0 should be 1
%       use n not n+1, do NOT apply to x_0 (which should be 1)
% Do:
%   normal_X = [X - mean(X)] / standard_deviation(X)
%   feature scaling make X to a new range of just 1,
%   mean normalization make X to a new average value of just 0
%
% Return: normal_X avg sigma

    avg = mean(X);
    sigma = std(X);

    normal_X = (X - avg) ./ sigma;

end
