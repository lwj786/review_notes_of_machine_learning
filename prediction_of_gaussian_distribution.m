function p = prediction_of_gaussian_distribution(X, mu, C)

% Func: prediction_of_gaussian_distribution()
%
% Parameter:
%   X -- k × n matrix, test data set
%   mu -- n dimension row vector, sample mean
%   C -- n × n symmetric matrix, sample covariance, or just n dimension vector, sample variance
%
% Do:
%   use multivariate gaussian distribution to predict the test data
%   if C was be set as smaple variance, C will be set as a diagonal matrix whose diagonal elements
%   are just sample variance. It assumes that different feature of X don't have linear relationship
%   with each other.
%
% Return:
%   p -- probability

    p = 0;

    n = length(mu);

    if size(C, 1) == 1 || size(C, 2) == 1
        C = diag(C);
    end

    p = 1 / ((2 * pi)^(n/2) * det(C)^(1/2)) ...
        * exp(-(1/2) * (X - mu) * inv(C) * (X - mu)');

    p = diag(p);

end
