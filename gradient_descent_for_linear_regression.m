function [theta J_history] = gradient_descent_for_linear_regression(theta, X, y, ...
    alpha, iteration_num, ...
    lambda = 0, b = 0)

% Func: gradient_descent_for_linear_regression(theta, X, y, alpha, iteration_num, lambda)
%
% Parameter:
%   theta -- the parameters of linear function: hypothesis
%       n+1 × 1 matrix
%   X -- the feature of data set
%       m × n+1 matrix, the first column x_0 should be 1
%   y -- the target value
%       m × 1 matrix
%   alpha -- step size
%   iteration_num -- number of iteration
%   lambda -- the regularization parameter (optional, default 0)
%   b -- the size of data in once gradient descent (optional, if want to set, should set lambda first),
%       define, if b <= 0 or b >= m, set b = m, batch gradient descent;
%               else if 1 < b < m, mini-batch gradient descent;
%               else if b = 1, stochastic gradient descent
%
% Do:
%   theta_j = theta_j -
%       alpha * {1/m * Σ[(h(x_i) - y_i) * x_i,j] + lambda / m * theta_j},
%           i = 1 -> m, j = 0 -> n (in regular term j = 1 -> n, not regularize the theta_0)
%   where h(x) = X * theta
%
% Return: theta J_history

    m = length(y);    % the number of examples

    J_history = zeros(iteration_num, 1);

    if b <= 0 || b >= m
        b = m;
    end

    index_start = index_end = 1;
    for count = 1:iteration_num
        while index_start < m
            index_end = index_start + b - 1;
            if index_end > m
                index_end = m;
                b = m - index_start + 1;
            end
            X_sub = X(index_start:index_end, :);
            y_sub = y(index_start:index_end, :);
            index_start = index_end + 1;

            theta = theta - alpha * (1/b) * X_sub' * (X_sub * theta - y_sub) ...
                - alpha * [0; lambda / b * theta(2:end, :)];   % regular term
        end

        J_history(count) = cost_function_for_linear_regression(theta, X, y, lambda);
    end

end
