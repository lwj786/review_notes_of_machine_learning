review notes of [a machine learning class at Coursera](https://www.coursera.org/learn/machine-learning)
 in the form of Octave code

##### supervised learning
###### linear regression:
* cost_function_for_linear_regression.m
* gradient_descent_for_linear_regression.m <br>
*in fact, the form of gradient of linear regression and that of logistic regression are the same.*
* normal_equation.m

###### logistic regression:
* cost_and_gradient_for_logistic_regression.m
* minimize_cost_for_logistic_regression.m

###### artificial neural networks:
* neural_networks_cost_and_gradient_function.m
* minimize_neural_networks_cost.m
* neural_networks_model_predict.m
* randomly_initialize_weights.m

###### diagnosis for machine learning:
* evaluating_model.m
* learning_curve.m
* error_metrics_for_skewed_data.m

###### support vector machine:
* cost_for_support_vector_machine.m
* gaussian_kernel.m

##### unsupervised learning
###### K-means clustering:
* K_means_clustering.m
* cost_for_K_means_clustering.m

###### principal component analysis:
* principal_component_analysis.m
* recover_data.m

###### density estimation:
* gaussian_distribution_for_density_estimation.m
* multivariate_gaussian_distribution_for_density_estimation.m
* prediction_of_gaussian_distribution.m

##### public function:
* normalize_feature.m
* sigmoid.m
* check_gradients.m
