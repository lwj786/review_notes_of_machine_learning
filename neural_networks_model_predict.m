function p = neural_networks_model_predict(nn_params, ...
    input_layer_size, hidden_layer_size, num_labels, ...
    X)

% Func: neural_networks_model_predict()
%
% Parameter:
%   nn_params -- or weights, a column vector
%   input_layer_size -- number of iuput units, should be a number
%   hidden_layer_size -- could be a number or vector
%       the length is number of hidden layers, and each value is number of units
%   num_labels -- or output layer size, should be a number
%   X -- the feature of data set to be predicted
%
% Do: use nn_params to predit the possibility to label
%
% Return: p

    m = size(X, 1);
    p = zeros(m, num_labels);

    layer_size = [input_layer_size(:); hidden_layer_size(:); num_labels(:)];
    num_layers = length(layer_size);

    a_l = [ones(m, 1) X];
    Theta_l = ...
        reshape(nn_params(1:(layer_size(1)  + 1) * layer_size(2)), ...
        layer_size(1) + 1, layer_size(2));
    Theta_num = (layer_size(1) + 1) * layer_size(2);

    for l = 2:num_layers
        a_l = sigmoid(a_l * Theta_l);
        if l ~= num_layers
            a_l = [ones(m, 1) a_l];    % the extra ones vector is bias
        end

        if l ~= num_layers
            Theta_l = ...
                reshape(nn_params(Theta_num + 1:Theta_num + (layer_size(l) + 1) * layer_size(l + 1)), ...
                    layer_size(l) + 1, layer_size(l + 1));
            Theta_num = Theta_num + (layer_size(l) + 1) * layer_size(l + 1);
        end
    end

    p = a_l;

end
